#include "connector.h"

Connector::Connector(QObject *parent)
    : QObject(parent)
    , mConfigurationWindow(new ConfigurationWindow())
    , mMainWindow(new MainWindow())
    , mResultView(new ResultsWindow())
{
    mConfigurationWindow->show();
}

void Connector::connectThemAll()
{
    connect(mMainWindow, SIGNAL(listConditionsForTwoVariablesUpdated(QList<QList<QLineEdit*>*>*)), mResultView, SLOT(onListConditionsForTwoVariablesUpdated(QList<QList<QLineEdit*>*>*)));
    connect(mMainWindow, SIGNAL(signConditionsUpdated(QList<QRadioButton*>*)), mResultView, SLOT(onSignConditionsUpdated(QList<QRadioButton*>*)));
    connect(mMainWindow, SIGNAL(resultTablesNeeded(QList<QTableWidget*>*)), mResultView, SLOT(onResultTablesNeeded(QList<QTableWidget*>*)));
    connect(mMainWindow, SIGNAL(configurationWindowNeeded()), mConfigurationWindow, SLOT(onConfigurationWindowNeeded()));
    connect(mConfigurationWindow, SIGNAL(calculatorWindowNeeded()), mMainWindow, SLOT(onCalculatorWindowNeeded()));
    connect(mConfigurationWindow, SIGNAL(variablesCountChanged(int)), mMainWindow, SLOT(setVariablesCount(int)));
    connect(mConfigurationWindow, SIGNAL(equationsCountChanged(int)), mMainWindow, SLOT(setEquationsCount(int)));
}


