#include "ui_resultswindow.h"
#include "resultswindow.h"

#include <cfloat>

ResultsWindow::ResultsWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResultsWindow)
    , listTablesWidget(new QList<QTableWidget*>())
    , mEquationsValue(new QVector<double>())
    , mLessThanSign(new QVector<bool>())
{
    ui->setupUi(this);
    connectButtons();
}

ResultsWindow::~ResultsWindow()
{
    delete ui;
}

void ResultsWindow::clearLayout()
{
    QLayoutItem* item;
    while ( ( item = ui->gridLayoutTable->takeAt( 0 ) ) != NULL )
    {
        delete item->widget();
        delete item;
    }
}

void ResultsWindow::onResultTablesNeeded(QList<QTableWidget*> *listTablesWidget)
{
    this->listTablesWidget = listTablesWidget;
    numberTable = 0;
    checkCountTable();
    ui->labelTablesName->setText(this->listTablesWidget->value(numberTable)->windowTitle());

    clearLayout();

    for (QTableWidget* tableWidget : *listTablesWidget) {
        ui->gridLayoutTable->addWidget(tableWidget);
    }

    setItemToWidget();

    ui->graphButton->setEnabled(false);
    this->show();
}

void ResultsWindow::onListConditionsForTwoVariablesUpdated(QList<QList<QLineEdit *> *> *listConditions)
{
    mEquationsValue->clear();
    for (auto equation : *listConditions) {
        for (QLineEdit* lineEdit : *equation) {
            mEquationsValue->append(lineEdit->text().toDouble());
        }
    }

    ui->graphButton->setEnabled(true);
}

void ResultsWindow::onSignConditionsUpdated(QList<QRadioButton *> *signConditions)
{
    for (QRadioButton* btn : *signConditions) {
        mLessThanSign->append(btn->isChecked());
    }
}

void ResultsWindow::onFirstTableButtonClicked()
{
    numberTable = 0;
    setItemToWidget();
}

void ResultsWindow::onLastTableButtonClicked()
{
    numberTable = listTablesWidget->size() - 1;
    setItemToWidget();
}

double ResultsWindow::getParameter(int equationsId, int variableId)
{
    return mEquationsValue->at(equationsId*3 + variableId);
}

QPair<double, double> ResultsWindow::getSolution(int equationId1, int equationId2)     // numery rownan, ktorych uklad rozwiazujemy
{
    double x,y;

    double a = getParameter(equationId1, 0);
    double b = getParameter(equationId1, 1);
    double c = getParameter(equationId1, 2);
    double d = getParameter(equationId2, 0);
    double e = getParameter(equationId2, 1);
    double f = getParameter(equationId2, 2);

    if (a == 0 || ((-d*b)/a + e) == 0) {
        x = 0;
        y = 0;
    } else {
        y = (f - (d*c)/a) / ((-d*b)/a + e);
        x = c/a - (b*y)/a;
    }

    return QPair<double,double>(x,y);
}

QVector<double> ResultsWindow::getGraphRange()
{
    double minX = DBL_MAX;
    double maxX = -DBL_MAX;
    double minY = DBL_MAX;
    double maxY = -DBL_MAX;

    QVector<double> range;

    int equationsCount = mEquationsValue->size()/3;

    for (int firstEquation = 0; firstEquation < equationsCount; firstEquation++) {
        for (int secondEquation = 0; secondEquation < equationsCount; secondEquation++) {
            QPair<double, double> solution = getSolution(firstEquation, secondEquation);

            if (solution.first < minX ) minX = solution.first;
            if (solution.first > maxX ) maxX = solution.first;
            if (solution.second < minY ) minY = solution.second;
            if (solution.second > maxY ) maxY = solution.second;
        }
    }

    range.append(minX - 5);
    range.append(maxX + 5);
    range.append(minY - 5);
    range.append(maxY + 5);

    return range;
}

void ResultsWindow::colourGraph(QVector<double> range, QCustomPlot* plot)
{
    int equationsCount = mEquationsValue->size()/3;

    QVector<double> xx,yy;

    for (double x = range[0]; x < range[1]; x+=0.01*(range[1]-range[0])) {
        for (double y = range[2]; y < range[3]; y+=0.01*(range[3]-range[2])) {

            bool colourMe = true;

            for (int i = 0; i < mEquationsValue->size()/3; i++) {
                double a = getParameter(i,0);
                double b = getParameter(i,1);
                double c = getParameter(i,2);

                if (mLessThanSign->at(i)) {
                    if (a*x + b*y > c) colourMe = false;
                } else {
                    if (a*x + b*y < c) colourMe = false;
                }
            }
            if (colourMe) {
                xx.append(x);
                yy.append(y);
            }
        }
    }
    plot->addGraph();
    plot->graph(equationsCount)->setData(xx, yy);

    plot->graph(equationsCount)->setLineStyle(QCPGraph::lsNone);
    plot->graph(equationsCount)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 1));
}

void ResultsWindow::onGraphButtonClicked()
{
    QWidget* graphWindow = new QWidget();
    graphWindow->setWindowTitle("Prezentacja rozwiązania");
    QCustomPlot* plot = new QCustomPlot(graphWindow);
    plot->setGeometry(0,0,graphWindow->width(), graphWindow->height());

    QVector<double> range = getGraphRange();
    QVector<double> x(101), y(101); // initialize with entries 0..100

    for (int i = 0; i < mEquationsValue->size()/3; i++) {

        double a = getParameter(i,0);
        double b = getParameter(i,1);
        double c = getParameter(i,2);

        for (int j=0; j<101; j++)
        {
          x[j] = range[0] + ((double)j)/100.0 * (range[1] - range[0]); // x goes from -1 to 1
          y[j] = (-1*a*x[j])/b + c/b;
        }
        plot->addGraph();
        plot->graph(i)->setData(x, y);
    }

    plot->xAxis->setLabel("x");
    plot->yAxis->setLabel("y");

    plot->xAxis->setRange(range[0], range[1]);
    plot->yAxis->setRange(range[2], range[3]);

    colourGraph(range, plot);

    plot->replot();

    graphWindow->show();
}

void ResultsWindow::onPreviousTableButtonClicked()
{
    numberTable--;
    setItemToWidget();
}

void ResultsWindow::onNextTableButtonClicked()
{
    numberTable++;
    setItemToWidget();
}

void ResultsWindow::checkCountTable()
{
    if(numberTable==0)
    {
        ui->previousTableButton->setEnabled(false);
        ui->firstTableButton->setEnabled(false);
        if(listTablesWidget->size()-1==0)
        {
            ui->nextTableButton->setEnabled(false);
            ui->lastTableButton->setEnabled(false);
        }
        else
        {
            ui->nextTableButton->setEnabled(true);
            ui->lastTableButton->setEnabled(true);
        }
    }
    else if(numberTable==listTablesWidget->size()-1)
    {
        ui->nextTableButton->setEnabled(false);
        ui->lastTableButton->setEnabled(false);

        ui->previousTableButton->setEnabled(true);
        ui->firstTableButton->setEnabled(true);
    }
    else
    {
        ui->previousTableButton->setEnabled(true);
        ui->nextTableButton->setEnabled(true);
        ui->firstTableButton->setEnabled(true);
        ui->lastTableButton->setEnabled(true);
    }
}

void ResultsWindow::setItemToWidget()
{
    for (QTableWidget* tableWidget : *listTablesWidget) {
        tableWidget->hide();
    }

    listTablesWidget->value(numberTable)->show();

    ui->labelTablesName->setText(this->listTablesWidget->value(numberTable)->windowTitle());
    checkCountTable();
}

void ResultsWindow::connectButtons()
{
    connect(ui->previousTableButton, SIGNAL(clicked(bool)), this, SLOT(onPreviousTableButtonClicked()));
    connect(ui->firstTableButton, SIGNAL(clicked(bool)), this, SLOT(onFirstTableButtonClicked()));
    connect(ui->nextTableButton, SIGNAL(clicked(bool)), this, SLOT(onNextTableButtonClicked()));
    connect(ui->lastTableButton, SIGNAL(clicked(bool)), this, SLOT(onLastTableButtonClicked()));
    connect(ui->graphButton, SIGNAL(clicked(bool)), this, SLOT(onGraphButtonClicked()));
}
