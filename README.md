Repozytorium zawiera program do rozwiązywania problemów liniowych za pomocą metody Simplex.
Tworzony na potrzeby zaliczenia przedmiotu Zaawansowane Metody Numeryczne.

Program został napisany w Qt Creator (C++).

Wprowadzanie danych:
- wpisywanie równań w formularzu,
- wybór rodzaju obliczeń (minimalizacja/maksymalizacja) z list lub checkboxów na formularzu.

Działanie:
- użytkownik może wybrać minimalizację lub maksymalizację funkcji celu,
- dla dwóch zmiennych możliwe jest wyrysowanie wykresu rozwiązania.

Założenia i ograniczenia:
- użytkownik ma możliwość wprowadzenia nierówności,
- program wykonuje maksymalnie 20 iteracji wyliczania metodą simplex, ze względu na pominięcie błędnych danych z których nie da się go wyliczyć.