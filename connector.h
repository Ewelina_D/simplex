#ifndef CONNECTOR_H
#define CONNECTOR_H

#include <QObject>

#include "configurationwindow.h"
#include "resultswindow.h"
#include "mainwindow.h"

class Connector : public QObject
{
    Q_OBJECT

public:
    explicit Connector(QObject *parent = 0);
    void connectThemAll();

private:
    ConfigurationWindow* mConfigurationWindow;
    MainWindow* mMainWindow;
    ResultsWindow *mResultView;
};

#endif // CONNECTOR_H
