#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mainwindow.h"

#include <QMainWindow>

namespace Ui {
    class ConfigurationWindow;
}

class ConfigurationWindow : public QMainWindow
{
    Q_OBJECT

    Q_PROPERTY(int variablesCount READ variablesCount WRITE setVariablesCount NOTIFY variablesCountChanged)
    Q_PROPERTY(int equationsCount READ equationsCount WRITE setEquationsCount NOTIFY equationsCountChanged)

public:
    explicit ConfigurationWindow(QWidget *parent = 0);
    ~ConfigurationWindow();

    int variablesCount() const;
    int equationsCount() const;

public Q_SLOTS:
    void onConfigurationWindowNeeded();

private Q_SLOTS:
    void setVariablesCount(int variablesCount);
    void setEquationsCount(int equationsCount);
    void onVariablesCountEditingFinished();
    void onEquationsCountEditingFinished();
    void onContinueButtonClicked();

Q_SIGNALS:
    void variablesCountChanged(int variablesCount);
    void equationsCountChanged(int equationsCount);
    void calculatorWindowNeeded();

private:
    void connectComponents();
    void tagBlanks();

    Ui::ConfigurationWindow* ui;
    MainWindow* w;
    int m_variablesCount;
    int m_equationsCount;

protected:
    bool eventFilter(QObject *watched, QEvent *event);
};

#endif // MAINWINDOW_H
