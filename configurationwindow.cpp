#include "ui_configurationwindow.h"
#include "configurationwindow.h"
#include "mainwindow.h"

#include <QMessageBox>
#include <QLineEdit>

ConfigurationWindow::ConfigurationWindow(QWidget *parent) :
    QMainWindow(parent)
    , ui(new Ui::ConfigurationWindow)
    , m_variablesCount(0)
    , m_equationsCount(0)
{
    ui->setupUi(this);

    connectComponents();

    ui->variablesCountLineEdit->setValidator(new QIntValidator(this));
    ui->variablesCountLineEdit->setAlignment(Qt::AlignCenter);

    ui->equationsCountLineEdit->setValidator(new QIntValidator(this));
    ui->equationsCountLineEdit->setAlignment(Qt::AlignCenter);

    ui->variablesCountLineEdit->installEventFilter(this);
    ui->equationsCountLineEdit->installEventFilter(this);
}

ConfigurationWindow::~ConfigurationWindow()
{
    delete ui;
}

int ConfigurationWindow::variablesCount() const
{
    return m_variablesCount;
}

int ConfigurationWindow::equationsCount() const
{
    return m_equationsCount;
}

void ConfigurationWindow::onConfigurationWindowNeeded()
{
    setVariablesCount(0);
    setEquationsCount(0);

    ui->variablesCountLineEdit->clear();
    ui->equationsCountLineEdit->clear();

    this->show();
}

void ConfigurationWindow::setVariablesCount(int variablesCount)
{
    if (m_variablesCount == variablesCount)
        return;

    m_variablesCount = variablesCount;
    emit variablesCountChanged(variablesCount);
}

void ConfigurationWindow::setEquationsCount(int equationsCount)
{
    if (m_equationsCount == equationsCount)
        return;

    m_equationsCount = equationsCount;
    emit equationsCountChanged(equationsCount);
}

void ConfigurationWindow::onVariablesCountEditingFinished()
{
    setVariablesCount(ui->variablesCountLineEdit->text().toInt());
}

void ConfigurationWindow::onEquationsCountEditingFinished()
{
    setEquationsCount(ui->equationsCountLineEdit->text().toInt());
}

void ConfigurationWindow::onContinueButtonClicked()
{
    if (ui->equationsCountLineEdit->text().isEmpty() || ui->variablesCountLineEdit->text().isEmpty()) {
        tagBlanks();
    } else if(variablesCount() == 0 || equationsCount() == 0) {
        QMessageBox msgBox;
        msgBox.setText("Wpisane wartości muszą być większe od 0.");
        msgBox.exec();
    } else {
        emit calculatorWindowNeeded();
        this->close();
    }

}

void ConfigurationWindow::connectComponents()
{
    connect(ui->continueButton, SIGNAL(clicked(bool)), this, SLOT(onContinueButtonClicked()));
    connect(ui->variablesCountLineEdit, SIGNAL(editingFinished()), this, SLOT(onVariablesCountEditingFinished()));
    connect(ui->equationsCountLineEdit, SIGNAL(editingFinished()), this, SLOT(onEquationsCountEditingFinished()));
}

void ConfigurationWindow::tagBlanks()
{
    if (ui->variablesCountLineEdit->text().isEmpty()) {
        ui->variablesCountLineEdit->setStyleSheet("border: 1px solid red");
    }
    if (ui->equationsCountLineEdit->text().isEmpty()){
        ui->equationsCountLineEdit->setStyleSheet("border: 1px solid red");
    }
}

bool ConfigurationWindow::eventFilter(QObject *watched, QEvent *event)
{
    // focusIn event
    if (event->type() == event->FocusIn && watched == ui->variablesCountLineEdit) {
        ui->variablesCountLineEdit->setStyleSheet(styleSheet());
    }

    else if (event->type() == event->FocusIn && watched == ui->equationsCountLineEdit) {
        ui->equationsCountLineEdit->setStyleSheet(styleSheet());
    }

    // focusOut event
    else if (event->type() == event->FocusOut && watched == ui->variablesCountLineEdit) {
        if (ui->variablesCountLineEdit->text().isEmpty()) {
            ui->variablesCountLineEdit->setStyleSheet("border: 1px solid red");
        }
    }

    else if (event->type() == event->FocusOut && watched == ui->equationsCountLineEdit) {
        if (ui->equationsCountLineEdit->text().isEmpty()) {
            ui->equationsCountLineEdit->setStyleSheet("border: 1px solid red");
        }
    }

    return QMainWindow::eventFilter(watched, event);
}
