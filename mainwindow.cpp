#include "ui_configurationwindow.h"
#include "mainwindow.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
    , m_variablesCount(0)
    , m_equationsCount(0)
    , mBackToConfigurationButton(new QPushButton(this))
    , mCalculateButton(new QPushButton(this))
{
    listNameValiables = new QList<QString>;
    listValiablesFunction = new QList<QLineEdit*>;
    listConditions = new QList<QList<QLineEdit*>*>;
    min_max = new QList<QRadioButton*>;
    signConditions = new QList<QRadioButton*>;

    connect(mBackToConfigurationButton, SIGNAL(clicked(bool)), this, SLOT(onBackToConfigurationButtonClicked()));
    connect(mCalculateButton, SIGNAL(clicked(bool)), this, SLOT (onCalculateButtonClicked()));
}

void MainWindow::createLayout()
{
    clearLayout();

    QGridLayout *layout= new QGridLayout;

    QList<QHBoxLayout*> *layoutFunction = createLayoutFunction(variablesCount());
    QList<QHBoxLayout*> *layoutConditions = createLayoutConditions(variablesCount(), equationsCount());

    layout->addLayout(layoutFunction->value(0),0,0);
    layout->addLayout(layoutFunction->value(1),1,0);
    layout->addLayout(createLayoutMinMax(),2,0);

    for(int i=0;i<equationsCount()+1;i++)
    {
        layout->addLayout(layoutConditions->value(i),3+i,0);
    }

    QVBoxLayout *layoutButton = new QVBoxLayout;
    mCalculateButton->setText("Licz...");
    layoutButton->addWidget(mCalculateButton);

    mBackToConfigurationButton->setText("Powrót do konfiguracji");
    layoutButton->addWidget(mBackToConfigurationButton);

    layout->addLayout(layoutButton,equationsCount()+3+1,0);

    QWidget* centralWidget = new QWidget();
    centralWidget->setLayout(layout);
    setCentralWidget(centralWidget);
}

QList<QHBoxLayout*> *MainWindow::createLayoutFunction(int val)
{
    QList<QHBoxLayout*> *layouts = new QList<QHBoxLayout*>;
    QHBoxLayout *layout1 = new QHBoxLayout;
    QHBoxLayout *layout2 = new QHBoxLayout;
    for(int i=0;i<val;i++)
    {
        QString nameValiable = "x"+QString::number(i+1);
        //listNameValiables->append(nameValiable);
        QLabel *label = new QLabel(nameValiable);
        label->setAlignment(Qt::AlignCenter);
        label->setMaximumHeight(10);
        layout1->addWidget(label);
        QLineEdit *lineEdit = new QLineEdit;
        lineEdit->setValidator(new QDoubleValidator(this));
        lineEdit->setAlignment(Qt::AlignCenter);
        listValiablesFunction->append(lineEdit);
        layout2->addWidget(lineEdit);
    }
    layouts->append(layout1);
    layouts->append(layout2);
    return layouts;
}



QList<QHBoxLayout*> *MainWindow::createLayoutConditions(int val, int con)
{
    QList<QHBoxLayout*> *layouts = new QList<QHBoxLayout*>;
    QHBoxLayout *layout1 = new QHBoxLayout;
    for(int i=0;i<val+2;i++)
    {
        QString nameValiable;
        if(i==val+1)
        {
            nameValiable = "wartość";
        }
        else if(i>=val)
        {
            nameValiable = "";
        }
        else
        {
            nameValiable = "x"+QString::number(i+1);
        }
        QLabel *label = new QLabel(nameValiable);
        label->setAlignment(Qt::AlignCenter);
        label->setMaximumHeight(10);
        layout1->addWidget(label);
    }
    layouts->append(layout1);
    for(int i=0;i<con;i++)
    {
        QHBoxLayout *layout2 = new QHBoxLayout;
        QList<QLineEdit*> *listLineEdit = new QList<QLineEdit*>;
        for(int j=0;j<val+2;j++)
        {
            if(j==val)
            {
                QGroupBox *groupBox = new QGroupBox(tr(""));
                QRadioButton *radioButton1  = new QRadioButton(tr("<="));
                radioButton1->setChecked(true);
                signConditions->append(radioButton1);
                QRadioButton *radioButton2 = new QRadioButton(tr(">="));
                QVBoxLayout *vbox = new QVBoxLayout;
                vbox->addWidget(radioButton1);
                vbox->addWidget(radioButton2);
                vbox->addStretch(1);
                groupBox->setLayout(vbox);
                layout2->addWidget(groupBox);
            }
            else
            {
                QLineEdit *lineEdit = new QLineEdit;
                lineEdit->setValidator(new QDoubleValidator(this));
                lineEdit->setAlignment(Qt::AlignCenter);
                listLineEdit->append(lineEdit);
                layout2->addWidget(lineEdit);
            }
        }
        listConditions->append(listLineEdit);
        layouts->append(layout2);
    }

    return layouts;
}

QHBoxLayout *MainWindow::createLayoutMinMax()
{
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(new QLabel);
    QRadioButton *min = new QRadioButton("MIN");
    min->setChecked(true);
    min_max->append(min);
    layout->addWidget(min);
    QRadioButton *max = new QRadioButton("MAX");
    min_max->append(max);
    layout->addWidget(max);
    layout->setAlignment(Qt::AlignCenter);
    return layout;
}

int MainWindow::variablesCount() const
{
    return m_variablesCount;
}

int MainWindow::equationsCount() const
{
    return m_equationsCount;
}

void MainWindow::setVariablesCount(int variablesCount)
{
    if (m_variablesCount == variablesCount)
        return;

    m_variablesCount = variablesCount;
    emit variablesCountChanged(variablesCount);
}

void MainWindow::setEquationsCount(int equationsCount)
{
    if (m_equationsCount == equationsCount)
        return;

    m_equationsCount = equationsCount;
    emit equationsCountChanged(equationsCount);
}

void MainWindow::onCalculatorWindowNeeded()
{
    createLayout();
    this->show();
}

void MainWindow::onBackToConfigurationButtonClicked()
{
    setVariablesCount(0);
    setEquationsCount(0);

    this->close();
    emit configurationWindowNeeded();
}

void MainWindow::onCalculateButtonClicked()
{
    bool hasAnyEmptyField = false;

    QList<QLineEdit*> allLineEdits = this->findChildren<QLineEdit*>();

    for (QLineEdit* lineEdit : allLineEdits) {
        if (lineEdit->text().isEmpty()) {
            hasAnyEmptyField = true;
        }
    }

    if (hasAnyEmptyField) {
        QMessageBox msgBox;
        msgBox.setText("Puste pola zostaną automatycznie uzupełnione zerami.");
        QPushButton *okButton = msgBox.addButton("Kontynuuj obliczenia", QMessageBox::AcceptRole);
        QPushButton *cancelButton = msgBox.addButton("Uzupełnij dane", QMessageBox::RejectRole);

        msgBox.exec();

        if (msgBox.clickedButton() == okButton) {
            calculate();
        } else if (msgBox.clickedButton() == cancelButton) {
           msgBox.close();
        }
    } else {
        calculate();
    }

    if (variablesCount() == 2) {
        emit listConditionsForTwoVariablesUpdated(listConditions);
        emit signConditionsUpdated(signConditions);
    }
}

void MainWindow::createTableSimplex()
{
    double val = 0;
    CbString = new QVector<QString>;
    CbValues = new QVector<double>;
    Zj = new QVector<double>;
    Cj_Zj = new QVector<double>;

    for(int i = 0;i<variablesCount()+equationsCount();i++)
    {
        QString nameValiable = "x"+QString::number(i+1);
        listNameValiables->append(nameValiable);

        if(i>= variablesCount())
        {
            CbString->append(nameValiable);
            CbValues->append(val);
            values->append(val);
            for(int j = 0;j<equationsCount();j++)
            {
                double last = table->value(j)->takeLast();
                if(j==i-m_variablesCount)
                {
                    table->value(j)->append(1);
                }
                else
                {
                    table->value(j)->append(val);
                }
                table->value(j)->append(last);
            }
        }
        Zj->append(val);
    }
    Zj->append(val);
    for(int i=0;i<values->size();i++)
    {
        Cj_Zj->append(values->value(i)-Zj->value(i));
    }
}

void MainWindow::simplex()
{
    printTable(1);
    bool win = false;
    for(int i=0;i<Cj_Zj->size();i++)
    {
        if(Cj_Zj->value(i)>0)
        {
            break;
        }
        else if(i==Cj_Zj->size()-1)
        {
            win = true;
        }
    }
    k = 1;
    while(!win)
    {
        int selectColumn = 0;
        int selectCb = 0;
        for(int i=1;i<Cj_Zj->size();i++)
        {
            if(Cj_Zj->value(selectColumn)<Cj_Zj->value(i))
            {
                selectColumn = i;
            }
        }

        for(int i=0;i<CbValues->size();i++)
        {
            if((table->value(i)->value(table->value(i)->size()-1)/table->value(i)->value(selectColumn)) >= 0)
            {
                selectCb = i;
                break;
            }
        }

        for(int i=1;i<CbValues->size();i++)
        {
            if(qFabs(table->value(i)->value(table->value(i)->size()-1)/table->value(i)->value(selectColumn)) <
                    qFabs(table->value(selectCb)->value(table->value(selectCb)->size()-1)/table->value(selectCb)->value(selectColumn))&&
                        (table->value(i)->value(table->value(i)->size()-1)/table->value(i)->value(selectColumn) > 0))
            {
                selectCb = i;
            }
        }
        CbString->replace(selectCb,listNameValiables->value(selectColumn));
        CbValues->replace(selectCb,values->value(selectColumn));

        double consT = table->value(selectCb)->value(selectColumn);
        for(int i=0;i<table->value(selectCb)->size();i++)
        {
            table->value(selectCb)->replace(i,table->value(selectCb)->value(i)/consT);
        }
        for(int j=0;j<equationsCount();j++)
        {
            if(j!=selectCb)
            {
                consT = table->value(j)->value(selectColumn);
                for(int i=0;i<table->value(j)->size();i++)
                {
                    table->value(j)->replace(i,table->value(j)->value(i)-
                                             (consT*table->value(selectCb)->value(i)));
                }
            }
        }
        for(int i=0;i<Zj->size();i++)
        {
            double val = 0;
            for(int j=0;j<CbValues->size();j++)
            {
                val += CbValues->value(j)*table->value(j)->value(i);
            }
            Zj->replace(i,val);
        }

        for(int i=0;i<values->size();i++)
        {
            Cj_Zj->replace(i,values->value(i)-Zj->value(i));
        }

        for(int i=0;i<Cj_Zj->size();i++)
        {
            if(Cj_Zj->value(i)>0)
            {
                printTable(k+1);
                break;
            }
            else if(i==Cj_Zj->size()-1)
            {
                win = true;
                printTable(-1*(k+1));
            }
        }
        k++;
        if(k>20)
        {
            qDebug() << "Error";
            break;
        }
    }
}

void MainWindow::printTable(int number)
{
    QTableWidget *tableWidget = new QTableWidget;

    tableWidget->setColumnCount(3+variablesCount()+equationsCount());
    tableWidget->setRowCount(equationsCount()+4);
    tableWidget->verticalHeader()->setVisible(false);
    tableWidget->horizontalHeader()->setVisible(false);
    tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);


    tableWidget->setItem(0,variablesCount()+3 + equationsCount()-1,createCell("",Qt::gray));
    tableWidget->setItem(0,0,createCell("",Qt::gray));
    tableWidget->setItem(0,1,createCell("Cj",Qt::white));
    tableWidget->setItem(1,1,createCell("Zmienne bazowe",Qt::gray));
    tableWidget->setItem(1,0,createCell("Cb",Qt::gray));
    tableWidget->setItem(1,variablesCount()+3 + equationsCount()-1,createCell("Rozwiąanie (Bi)",Qt::gray));
    tableWidget->setItem(equationsCount()+4-2,1,createCell("Zj",Qt::gray));
    tableWidget->setItem(equationsCount()+4-2,0,createCell("",Qt::gray));
    tableWidget->setItem(equationsCount()+4-1,1,createCell("Cj-Zj",Qt::white));
    tableWidget->setItem(equationsCount()+4-1,0,createCell("",Qt::gray));
    tableWidget->setItem(equationsCount()+4-1,variablesCount()+3 + equationsCount()-1,createCell("",Qt::gray));

    for(int i=0;i<values->size();i++)
    {
        tableWidget->setItem(0,2+i,createCell(QString::number(values->value(i)),Qt::white));
        tableWidget->setItem(1,2+i,createCell(listNameValiables->value(i),Qt::gray));
    }
    for(int i=0;i<CbString->size();i++)
    {
        tableWidget->setItem(i+2,0,createCell(QString::number(CbValues->value(i)),Qt::white));
        tableWidget->setItem(i+2,1,createCell(CbString->value(i),Qt::white));
    }
    for(int j=0;j<m_equationsCount;j++)
    {
        for(int i=0;i<table->value(j)->size();i++)
        {
            tableWidget->setItem(j+2,i+2,createCell(QString::number(table->value(j)->value(i)),Qt::white));
        }
    }
    for(int i=0;i<Zj->size();i++)
    {
        tableWidget->setItem(equationsCount()+4-2,i+2,createCell(QString::number(Zj->value(i)),Qt::gray));
        if(i<Zj->size()-1)
        {
            tableWidget->setItem(equationsCount()+4-1,2+i,createCell(QString::number(Cj_Zj->value(i)),Qt::white));
        }
    }

    if(number<0)
    {
        tableWidget->setWindowTitle("Tabela Nr. "+QString::number(number*-1)+" -> tabela końcowa");
    }
    else
    {
        tableWidget->setWindowTitle("Tabela Nr. "+QString::number(number));
    }
    listTablesWidget->append(tableWidget);

}

QTableWidgetItem* MainWindow::createCell(QString string,QColor color)
{
    QTableWidgetItem* cell = new QTableWidgetItem(string);
    cell->setTextAlignment(Qt::AlignCenter);
    cell->setBackground(color);
    if(color == Qt::gray)
    {
        QFont font;
        font.setBold(true);
        font.setItalic(true);
        font.setPointSize(10);
        cell->setFont(font);
    }
    else
    {
        QFont font;
        font.setPointSize(11);
        cell->setFont(font);
    }
    return cell;
}

void MainWindow::clearData()
{

    listNameValiables->clear();

    table->clear();
    values->clear();
    CbString->clear();
    CbValues->clear();
    Zj->clear();
    Cj_Zj->clear();
}

void MainWindow::calculate()
{
    values = new QVector<double>;

    table = new QVector<QVector<double>*>;

    for(int i=0;i<variablesCount();i++)
    {
        if(min_max->value(0)->isChecked())
        {
            values->append(listValiablesFunction->value(i)->text().replace(",",".").toDouble()*-1);
        }
        else
        {
            values->append(listValiablesFunction->value(i)->text().replace(",",".").toDouble());
        }
    }

    for(int i=0;i<equationsCount();i++)
    {
        QVector<double> *_values = new QVector<double>;
        for(int j=0;j<variablesCount()+1;j++)
        {
            double value = listConditions->value(i)->value(j)->text().replace(",",".").toDouble();
            if(!signConditions->value(i)->isChecked())
            {
                value *= -1;
            }
            _values->append(value);
            //qDebug() << _values->value(j);
        }
        table->append(_values);
    }

    createTableSimplex();
    listTablesWidget =new QList<QTableWidget*>;
    simplex();
    if(k>20)
    {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Przy takich podanych danych nie można obliczyć! \nProgram wykonał ponad 20 literacji...");
        messageBox.setFixedSize(300,200);
    }
    else
    {
        if(listTablesWidget->size() == 1)
        {
            listTablesWidget->value(0)->setWindowTitle("Tabela Nr. 1 -> tabela końcowa");
        }
        resultTablesNeeded(listTablesWidget);
    }

    clearData();
}

void MainWindow::clearLayout()
{
    listConditions->clear();
    listValiablesFunction->clear();
    signConditions->clear();
    min_max->clear();
}
