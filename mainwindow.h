#ifndef MAINWINDOW2_H
#define MAINWINDOW2_H

#include <QTableWidgetItem>
#include <QRadioButton>
#include <QTableWidget>
#include <QMainWindow>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QGroupBox>
#include <QVector>
#include <QLabel>
#include <QtMath>
#include <QDebug>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    Q_PROPERTY(int variablesCount READ variablesCount WRITE setVariablesCount NOTIFY variablesCountChanged)
    Q_PROPERTY(int equationsCount READ equationsCount WRITE setEquationsCount NOTIFY equationsCountChanged)

public:
    explicit MainWindow(QWidget *parent = 0);

    int variablesCount() const;
    int equationsCount() const;

public Q_SLOTS:
    void setVariablesCount(int variablesCount);
    void setEquationsCount(int equationsCount);
    void onCalculatorWindowNeeded();

private Q_SLOTS:
    void onBackToConfigurationButtonClicked();
    void onCalculateButtonClicked();

Q_SIGNALS:
    void listConditionsForTwoVariablesUpdated(QList<QList<QLineEdit*>*>* listConditions);
    void signConditionsUpdated(QList<QRadioButton*>* signConditions );
    void variablesCountChanged(int variablesCount);
    void equationsCountChanged(int equationsCount);
    void configurationWindowNeeded();
    void resultTablesNeeded(QList<QTableWidget*>*);

private:
    int m_variablesCount;
    int m_equationsCount;

    QList<QList<QLineEdit*>*>* listConditions;
    QList<QLineEdit*>* listValiablesFunction;
    QPushButton* mBackToConfigurationButton;
    QList<QRadioButton*>* signConditions;
    QList<QString>* listNameValiables;
    QList<QRadioButton*>* min_max;
    QPushButton* mCalculateButton;

    QVector<QVector<double>*>* table;
    QVector<QString>* CbString;
    QVector<double>* CbValues;
    QVector<double>* values;
    QVector<double>* Cj_Zj;
    QVector<double>* Zj;
    int k;

    QList<QTableWidget*> *listTablesWidget;
    QTableWidget *firstTableWidget;

    QList<QHBoxLayout*>* createLayoutConditions(int val,int con);
    QList<QHBoxLayout*>* createLayoutFunction(int val);
    QHBoxLayout* createLayoutMinMax();
    QTableWidgetItem* createCell(QString string,QColor color);
    void printTable(int number);
    void createTableSimplex();
    void createLayout();
    void clearLayout();
    void clearData();
    void calculate();
    void simplex();
};

#endif // MAINWINDOW2_H
