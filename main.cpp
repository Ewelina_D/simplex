#include "configurationwindow.h"

#include <QApplication>

#include "connector.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Connector connector;
    connector.connectThemAll();

    return a.exec();
}
