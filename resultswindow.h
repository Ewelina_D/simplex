#ifndef FORMTABLES_H
#define FORMTABLES_H

#include "qcustomplot.h"

#include <QTableWidget>
#include <QWidget>
#include <QVector>
#include <QList>
#include <QPair>


namespace Ui {
    class ResultsWindow;
}

class ResultsWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ResultsWindow(QWidget *parent = 0);
    ~ResultsWindow();

public Q_SLOTS:
    void onListConditionsForTwoVariablesUpdated(QList<QList<QLineEdit*>*>* listConditions);
    void onSignConditionsUpdated(QList<QRadioButton*>* signConditions);
    void onResultTablesNeeded(QList<QTableWidget*> *listTablesWidget);

private Q_SLOTS:
    void onPreviousTableButtonClicked();
    void onFirstTableButtonClicked();
    void onNextTableButtonClicked();
    void onLastTableButtonClicked();
    void onGraphButtonClicked();

private:
    Ui::ResultsWindow *ui;

    QList<QTableWidget*>* listTablesWidget;
    QVector<double>* mEquationsValue;
    QVector<bool>* mLessThanSign;
    int numberTable;

    QPair<double, double> getSolution(int equationId1, int equationId2);
    void colourGraph(QVector<double> range, QCustomPlot *plot);
    double getParameter(int equationsId, int variableId);
    QVector<double> getGraphRange();
    void checkCountTable();
    void setItemToWidget();
    void connectButtons();
    void clearLayout();
};

#endif // FORMTABLES_H
