#-------------------------------------------------
#
# Project created by QtCreator 2016-12-21T10:17:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = SimplexCalculator
TEMPLATE = app


SOURCES +=  main.cpp\
            configurationwindow.cpp \
            mainwindow.cpp \
            connector.cpp \
            resultswindow.cpp \
            qcustomplot.cpp

HEADERS +=  configurationwindow.h \
            mainwindow.h \
            connector.h \
            resultswindow.h \
            qcustomplot.h

FORMS   +=  configurationwindow.ui \
            resultswindow.ui
